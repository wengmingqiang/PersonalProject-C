package work1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Main {

	public static void main(String[] args) throws Exception {
		
		File file = new File("d:\\1.txt");
		
		String content = ReadFile. readFileContent(file);
         System.out.println(content);
		int charNUM = Deal.getCharCount(content);
		System.out.println("字符数："+charNUM);
		int lineNum = Deal.getLineCount(content);
		System.out.println("行数："+lineNum);
		int wordsNum = Deal.getWordsNum(content);
		System.out.println("单词数："+wordsNum);
		
		Map<String, Integer> wordFreq = Deal.getWordFreq(content);
		/*遍历map*/
		for (Map.Entry<String, Integer> entry : wordFreq.entrySet()){
	         String key = entry.getKey();
	         Integer value = entry.getValue();
	         System.out.println("单词:" + key + ", 数量:" + value);
	  }

		
	}

}
