package work1;

import java.io.BufferedReader;
import java.util.HashMap;
import java.util.Map;

public class Deal {
	
	public static int getCharCount(String text) // 统计文件字符数(ascll码（32~126），制表符，换行符，)
	 {
	     char c;
	     int charNum = 0;
	     for (int i = 0; i < text.length(); i++) {
	         c = text.charAt(i); //把字符串转化为字符数组
	         if (c >= 32 && c <= 126 || c == '\r' || c == '\n'|| c == '\t') {
	             charNum++;
	         }
	     }
	     return charNum;
	 }
	
	
	 public static int getLineCount(String text)throws Exception  { // 统计有效行数

	
		 	int lineNum = 0;
		     String[] line = text.split("\r\n"); // 将每一行分开放入一个字符串数组
		     for (int i = 0; i < line.length; i++) { // 找出无效行，统计有效行

		         if (line[i].trim().length() == 0)
		             continue;
		         lineNum ++;
		     }
		     return lineNum;
		 }
	 public static String[] getWords(String text) {
		 
		 text = text.replace('\r', ' ');
		 text = text.replace('\r', ' ');
		 text = text.replace('\r', ' ');
		 
		 String [] words = text.split(" ");
		  
		 
		 return words;
		 
	 }
	 
	 
	 public static int getWordsNum(String text) {
		 
		 String content = text.replace('\r', ' ');
		 content = text.replace('\b', ' ');
		 content = text.replace('\n', ' ');
		 
		 String [] words = content.split(" ");
		 int wordCount = 0;
		 
		 for(int i= 0; i<words.length;i++)
		 {
			 if (words[i].length()<4)
				 continue;
			 
			 int j = 0;
			 for(  j =0;j<4;j++)
			 { 
				 char c =words[i].charAt(j);
				 if(!((c>='A'&& c<='Z')||(c>='a'&& c<='z')))
					 break;
				
			 }
			
			 if(j==4)
				 wordCount++;

			
		 }
		  
		 return wordCount;
		 
	 }
	 
	 
	 
	 public static Map<String, Integer> getWordFreq(String text) // 统计单词词频(单词：以4个英文字母开头，跟上字母数字符号，单词以分隔符分割，不区分大小写。)
	 {
		 HashMap<String, Integer> wordFreq = new HashMap<String, Integer>();
		 
		 String content = text.replace('\r', ' ');
		 content = text.replace('\b', ' ');
		 content = text.replace('\n', ' ');
		 
		 String [] words = content.split(" ");
		 
		 
		 for(int i= 0; i<words.length;i++)
		 {
			 if (words[i].length()<4)
				 continue;
			 
			 int j = 0;
			 for(  j =0;j<4;j++)
			 { 
				 char c =words[i].charAt(j);
				 if(!((c>='A'&& c<='Z')||(c>='a'&& c<='z')))
					 break;
			 }
			
			 if(j==4)
			 {
				 words[i] = words[i].trim().toLowerCase();  // 将字符串转化为小写
                 if (wordFreq.get(words[i]) == null) 
	                 { // 判断之前Map中是否出现过该字符串
	                     wordFreq.put(words[i], 1);
	                 } 
                 else
                     wordFreq.put(words[i], wordFreq.get(words[i]) + 1); 
			 }
		 }
	     return wordFreq;
	 }
	 
	 
	 
	 
	 
	 
}
