package work1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;

public class ReadFile {
/*public static void main(String[] args) throws IOException {
	String fileContent = readFileContent("");
	System.out.println(fileContent);
}
//参数string为你的文件名*/


public static String readFileContent(File file) throws Exception {

	FileInputStream fis = new FileInputStream(file);
	byte[] buf = new byte[1024];
	StringBuffer sb = new StringBuffer();
	while ((fis.read(buf)) != -1) {
		sb.append(new String(buf));
		buf = new byte[1024];// 重新生成，避免和上次读取的数据重复
	}
	return sb.toString();
	
	
	
}
}

